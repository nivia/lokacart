package com.mobile.ict.cart;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;



public class DraftOrderActivity extends Activity implements View.OnClickListener,ProcessOrderAsyncTaskListener {

    TextView heading;
    Bundle bundle;
    ListView orderList,orderListItem;
    DraftOrderItemAdapter adapter;
    DraftOrderListAdapter listadapter;
    ArrayList<ArrayList<String[]>> draftOrderlistitem ;
    String memberNumber,orgAbbr,orgId;
    Dialog dialog ;
    Window window;
    ArrayList<ItemRow> items;
    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    Point size;
    WindowManager windowManager;
    Display display;
    int width,height,dialogid,alertdialogid;
    ArrayList<DraftOrder> saved_order_list;
    private RetainedFragment dataFragment;
    Button delete,confirm;
    SharedPreferences.Editor editor;
    String item[];
    int pos;
    double grandtotal =0.0;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_orders);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();
        memberNumber=sharedPref.getString("mobilenumber", "");

        orgAbbr=sharedPref.getString("orgAbbr","");
        orgId=sharedPref.getString("orgIdSelected","");

        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
        heading=(TextView)findViewById(R.id.draft_order_label);
        heading.setTypeface(tf1);



        bundle = new Bundle();

        orderList = (ListView) findViewById(R.id.draft_orders_list);


        confirm = (Button) findViewById(R.id.button_order);
        delete=(Button) findViewById(R.id.button_delete);


        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("DraftOrderActivity");


        if(dataFragment==null)
        {
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "DraftOrderActivity").commit();

            dialogid=-1;
            alertdialogid=-1;
            dataFragment.setdialogId(dialogid);
            dataFragment.setAlertDialogId(alertdialogid);

            saved_order_list= getDraftOrders();

            adapter = new DraftOrderItemAdapter(DraftOrderActivity.this,R.layout.activity_draft_order_item,saved_order_list);
            orderList.setItemsCanFocus(false);
            orderList.setAdapter(adapter);



        }
        else
        {




            if(dataFragment.getAlertDialogId()==1)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(DraftOrderActivity.this);
                System.out.println("builder value is "+builder);
                builder.setTitle(R.string.label_alertdialog_No_Draft_Found).setMessage(R.string.label_alertdialog_Do_you_want_to_save_new_draft)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent i = new Intent(DraftOrderActivity.this, NewOrderActivity.class);
                                startActivity(i);
                                finish();

                            }
                        })
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();

                            }
                        });

                dialog = builder.show();

            }
            else
            {
                saved_order_list =dataFragment.getDraftlist();
                draftOrderlistitem=dataFragment.getDraftorderlistitem();
                adapter = new DraftOrderItemAdapter(DraftOrderActivity.this,R.layout.activity_draft_order_item,saved_order_list);
                orderList.setItemsCanFocus(false);
                orderList.setAdapter(adapter);


                if(dataFragment.getdialogId() == 1) {

                    openDialog(dataFragment.getPos());
                }



            }


        }

        orderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                dialogid = 1;



                pos = position;
                openDialog(pos);


            }
        });




    }







    public void openDialog(int position)
    {

        dialog = new Dialog(DraftOrderActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.activity_draft_order_list, (ViewGroup) findViewById(R.id.draft_order), false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(Layout);

        TextView grandTotal = (TextView)dialog.findViewById(R.id.draft_order_total_bill);


        orderListItem =  (ListView)Layout.findViewById(R.id.draft_order_list);

        grandtotal=0.0;
        for(int m=0;m<draftOrderlistitem.get(pos).size();m++)
        {

            grandtotal=grandtotal+Double.parseDouble(draftOrderlistitem.get(pos).get(m)[3]);

        }

        grandTotal.setText(""+grandtotal);


        listadapter = new DraftOrderListAdapter(DraftOrderActivity.this,draftOrderlistitem.get(pos));
        orderListItem.setAdapter(listadapter);

        window =dialog.getWindow();

        size = new Point();
        windowManager = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
            windowManager.getDefaultDisplay().getSize(size);

            width = size.x;
            height = size.y;
        }else{
            display = windowManager.getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();
        }



        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            lp.width=(int)(width/1.125);
            lp.height = (int)(height/1.5);

        } else {
            lp.width=width/2;
            lp.height = (int)(height/1.25);

        }



        lp.gravity = Gravity.CENTER;

        window.setAttributes(lp);

        dialog.setCancelable(false);
        dialog.show();
    }



    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

        dataFragment.setDraftlist(saved_order_list);
        dataFragment.setDraftorderlistitem(draftOrderlistitem);

        if(alertdialogid==1)
        {
            dataFragment.setAlertDialogId(alertdialogid);
        }
        else
        {

            if(dialogid==1) {
                dataFragment.setdialogId(dialogid);
                dataFragment.setPos(pos);
            }
            else
            {
                if(dialogid==-1)
                    dataFragment.setdialogId(dialogid);

            }
        }






    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        if(dialog!=null)dialog.dismiss();


    }

    public void close(View v)
    {
        dialogid=-1;
        dialog.dismiss();

    }

    public void cancelOrder(View v)
    {


        dialogid=-1;
        dialog.dismiss();




    }




    public void delete_confirmOrder(View v)
    {

        // TODO Auto-generated method stub
        ArrayList<String> selectedItems = new ArrayList<String>();
        grandtotal=0.0;

       int countPos=0;

        for (DraftOrder p :adapter.getBox()) {

            if (p.box) {

                countPos++;

                for(int j=0;j<draftOrderlistitem.get(p.position).size();j++) {


                    selectedItems.add("Name:" + draftOrderlistitem.get(p.position).get(j)[1] + "\nQuantity:" + draftOrderlistitem.get(p.position).get(j)[4] + "Price:" + draftOrderlistitem.get(p.position).get(j)[2] + "Total:" + draftOrderlistitem.get(p.position).get(j)[3]);

                }

                for (int m = 0; m < draftOrderlistitem.get(p.position).size(); m++) {

                    grandtotal = grandtotal + Double.parseDouble(draftOrderlistitem.get(p.position).get(m)[3]);

                }

            }
        }

        String[] outputStrArr = new String[selectedItems.size()];
        items = new ArrayList<ItemRow>();

        for (int i = 0; i < selectedItems.size(); i++) {
            outputStrArr[i] = selectedItems.get(i);
            int aa =outputStrArr[i].indexOf("\n");
            String name = outputStrArr[i].substring(5, aa);
            int aa1 =outputStrArr[i].indexOf("Quantity:");

            int aa2 = outputStrArr[i].indexOf("Price:");
            int aa3 = outputStrArr[i].indexOf("Total:");
            String quantity= outputStrArr[i].substring(aa1+9 , aa2);
            String price = outputStrArr[i].substring(aa2+6, aa3);
            String total = outputStrArr[i].substring(aa3+6);
            ItemRow item = new ItemRow(name , Double.parseDouble(quantity), Double.parseDouble(price),Double.parseDouble(total));
            items.add(item);
        }

        int length= outputStrArr.length;

        if(v == confirm){

            if(length == 0){
                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,
                        Toast.LENGTH_LONG).show();

            }
            else {
                Bundle b = new Bundle();

                b.putString("totalBill",""+grandtotal);
                b.putString("draftId",saved_order_list.get(pos).getDraft_id());
                b.putStringArray("selectedItems", outputStrArr);
                Intent intent = new Intent(getApplicationContext(),DraftToOrder.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        }
        if(v == delete){

            if(length == 0){
                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,
                        Toast.LENGTH_LONG).show();

            }
            else {

                OrdersDraftDBHelper dd = new OrdersDraftDBHelper(getBaseContext());
                int deletePos=0;
                for(DraftOrder p :adapter.getBox()){

                    if(p.box)
                    {
                        deletePos++;
                        dd.deleteDraftOrder(saved_order_list.get(p.position).getDraft_id(),orgAbbr,orgId,memberNumber);

                    }


                    if(deletePos==countPos)
                        Toast.makeText(DraftOrderActivity.this,R.string.label_toast_Draft_deleted_successfully, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),DraftOrderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }

            }




        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        MenuItem item_refresh = menu.findItem(R.id.action_refresh);

        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);

        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_home) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancelOrder(String number, int orderid) {



    }





    public ArrayList<DraftOrder> getDraftOrders()
    {

        ArrayList<ArrayList<String[]>> list = new ArrayList<ArrayList<String[]>>();
        ArrayList<DraftOrder> draftOrderlist = new ArrayList<DraftOrder>();
        draftOrderlistitem = new ArrayList<ArrayList<String[]>>();
        ArrayList<String[]> draftitem;
        String[] item;
        OrdersDraftDBHelper dd = new OrdersDraftDBHelper(getBaseContext());

        try{

            list= dd.selectdraftOrder(orgAbbr,orgId,memberNumber);
            if(!list.isEmpty()){

                int i;

                String draft_id="";
                String name="" ;


                String price="";
                String quantity="";
                String total="";

                String member_num;

                String timestamp="";

                for (int z=list.size()-1;z>=0;z--)
                {
                   i=0;

                    draftitem = new ArrayList<>();
                    for(int x=0;x<list.get(z).size();x++)
                    {
                          item = new String[7];
                         item[0]=draft_id=list.get(z).get(x)[0];
                         item[1]=name = list.get(z).get(x)[1];


                         item[2]=price = list.get(z).get(x)[2];
                         item[3]=total = list.get(z).get(x)[3];
                         item[4]=quantity = list.get(z).get(x)[4];

                          item[5]=member_num=list.get(z).get(x)[5];

                         item[6]=timestamp = list.get(z).get(x)[6];


                       draftitem.add(item);


                    }

                    draftOrderlistitem.add(draftitem);





                    draftOrderlist.add(new DraftOrder(i,draft_id,timestamp,false));
                    i++;






                }

            }
            else{
                alertdialogid=1;

                AlertDialog.Builder builder = new AlertDialog.Builder(DraftOrderActivity.this);
                System.out.println("builder value is "+builder);
                builder.setTitle(R.string.label_alertdialog_No_Draft_Found).setMessage(R.string.label_alertdialog_Do_you_want_to_save_new_draft)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent i = new Intent(DraftOrderActivity.this, NewOrderActivity.class);
                                startActivity(i);
                                finish();

                            }
                        })
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();

                            }
                        });
                dialog = builder.show();




            }


        }
        catch(Exception e){
            Toast.makeText(DraftOrderActivity.this,R.string.label_toast_No_Draft_Order, Toast.LENGTH_LONG).show();

        }
        return draftOrderlist;

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

}

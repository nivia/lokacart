package com.mobile.ict.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class DraftOrderListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<String[]> objects;


    DraftOrderListAdapter(Context context, ArrayList<String[]> productList) {
        ctx = context;
        objects = productList;

        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public String[] getItem(int position) {

        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.activity_draft_order_list_item, parent, false);
        }


        String items[] = getItem(position);

        ((TextView) view.findViewById(R.id.draft_order_quantityName)).setText(items[1]);
        ((TextView) view.findViewById(R.id.draft_order_quantity)).setText(items[4]);
        ((TextView) view.findViewById(R.id.draft_order_quantityBasePrice)).setText(items[2]);
        ((TextView) view.findViewById(R.id.draft_order_quantityTotalPrice)).setText(items[3]);



        return view;
    }


}


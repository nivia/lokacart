package com.mobile.ict.cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DraftOrderItemAdapter extends ArrayAdapter<DraftOrder> {

    private List<DraftOrder> orderList = new ArrayList<DraftOrder>();
    private ProcessOrderAsyncTaskListener callback;
    CancelOrderViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    SharedPreferences sharedPref;
    Bundle b;

    HashMap<String,Double> quantities = new HashMap<String, Double>();
    public DraftOrderItemAdapter(Context context, int resource) {super(context, resource);}

    public DraftOrderItemAdapter(Context context, int resource, List<DraftOrder> orderList) {
        super(context, resource, orderList);

        this.orderList = orderList;
        this.context = context;
        this.callback = (ProcessOrderAsyncTaskListener)context;
    }

    static class CancelOrderViewHolder {
        TextView draft_time;
        CheckBox chkBox;
        TextView draft_date;
    }

    @Override
    public void add(DraftOrder object) {
        orderList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public DraftOrder getItem(int index) {
        return this.orderList.get(index);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_draft_order_item, parent, false);
            viewHolder = new CancelOrderViewHolder();
            viewHolder.draft_time = (TextView) row.findViewById(R.id.draft_order_time);
            viewHolder.draft_date = (TextView) row.findViewById(R.id.draft_order_date);
            viewHolder.chkBox = (CheckBox) row.findViewById(R.id.draft_order_chkbx);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CancelOrderViewHolder)row.getTag();
        }

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        final DraftOrder order = getItem(position);

        Date time = null;
        try {

            String timeStamp[]= order.getTime_stamp().split("\\,");
            viewHolder.draft_time.setText(timeStamp[1]);

            time = new SimpleDateFormat("yyyy-MM-dd").parse(timeStamp[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM dd, yyyy").format(time);

        viewHolder.draft_date.setText(date);

        viewHolder.chkBox.setOnCheckedChangeListener(myCheckChangList);
        viewHolder.chkBox.setTag(position);
        viewHolder.chkBox.setChecked(order.box);


        return row;
    }


    ArrayList<DraftOrder> getBox() {
        ArrayList<DraftOrder> box = new ArrayList<DraftOrder>();
        for (DraftOrder p : orderList) {
            {
                box.add(p);

            }

        }
        return box;
    }

    CompoundButton.OnCheckedChangeListener myCheckChangList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getItem((Integer) buttonView.getTag()).box = isChecked;
            getItem((Integer) buttonView.getTag()).position=(Integer) buttonView.getTag();
        }
    };


}

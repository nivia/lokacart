package com.mobile.ict.cart;

import java.util.ArrayList;

/**
 * Created by root on 9/10/15.
 */
public class ProductCategory {
    public String categoryName;
    public ArrayList<OrderRow> productItems = new ArrayList<>();

    public ProductCategory(String name) {
        categoryName = name;
    }
}

package com.mobile.ict.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CancelOrderListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<String[]> objects;

    CancelOrderListAdapter(Context context, ArrayList productList) {
        ctx = context;
        objects = productList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.activity_cancel_order_list_item, parent, false);
        }

        String[] list = (String[]) getItem(position);

        ((TextView) view.findViewById(R.id.cancel_order_quantityName)).setText(list[0]);
        ((TextView) view.findViewById(R.id.cancel_order_quantity)).setText(list[2]);
        ((TextView) view.findViewById(R.id.cancel_order_group_quantityBasePrice)).setText(list[1]);
        ((TextView) view.findViewById(R.id.cancel_order_quantityTotalPrice)).setText(list[3]);


        return view;
    }

}


package com.mobile.ict.cart;



import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;


public class PreviousOrdersActivity extends Activity implements OnClickListener {

    TextView heading;
    Bundle bundle;
    Map <String,String> itemsMap = new TreeMap<String, String>();

    ListView orderList,orderListItem;
    PreviousOrdersItemAdapter adapter;
    PreviousOrderListAdapter listadapter;
    ArrayList<JSONObject> orderObjects;
    ArrayList<SavedOrder> orders;
    String memberNumber,abbr;
    Dialog dialog ;
    Window window;
    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    Point size;
    WindowManager windowManager;
    Display display;
    int width,height,dialogid,alertdialogid;
    ArrayList<SavedOrder> saved_order_list;
    private RetainedFragment dataFragment;
    int pos;
    SharedPreferences sharedPref;
    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_orders);

        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
        heading=(TextView)findViewById(R.id.label_order_list);
        heading.setTypeface(tf1);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        memberNumber=sharedPref.getString("mobilenumber","");
        abbr=sharedPref.getString("orgAbbr","");


        bundle = new Bundle();

        orderList = (ListView) findViewById(R.id.order_list);





        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("PreviousOrderActivity2");


        if(dataFragment==null)
        {
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "PreviousOrderActivity2").commit();
            dialogid=-1;
            alertdialogid=-1;
            dataFragment.setdialogId(dialogid);
            dataFragment.setAlertDialogId(alertdialogid);

            new FetchOrders(PreviousOrdersActivity.this, itemsMap).execute("null");

        }
        else
        {


            if(dataFragment.getAlertDialogId()==1)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(PreviousOrdersActivity.this);
                builder.setTitle(R.string.label_alertdialog_No_Previous_Orders_Found)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                finish();

                            }
                        });




                dialog = builder.show();

            }

            else
            {
                orderObjects= dataFragment.getList();
                saved_order_list = getOrders();
                adapter = new PreviousOrdersItemAdapter(PreviousOrdersActivity.this,R.layout.activity_previous_order_item,saved_order_list);
                orderList.setAdapter(adapter);

                if(dataFragment.getdialogId() == 1) {

                    openDialog(dataFragment.getPos());
                }
            }


        }





        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);

                saved_order_list = getOrders();

                adapter = new PreviousOrdersItemAdapter(PreviousOrdersActivity.this,R.layout.activity_previous_order_item,saved_order_list);
                orderList.setAdapter(adapter);
              //  unlockScreenOrientation();

            }

        };






        orderList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                dialogid=1;



                pos = position;
                openDialog(pos);







            }
        });





    }







    public void openDialog(int position)
    {

        dialog = new Dialog(PreviousOrdersActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.activity_previous_order_list, (ViewGroup) findViewById(R.id.previous_order), false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(Layout);

        SavedOrder order = orders.get(position);
        orderListItem =  (ListView)Layout.findViewById(R.id.previous_orderlist);


        listadapter = new PreviousOrderListAdapter(PreviousOrdersActivity.this,order.getItemsList(pos));
        orderListItem.setAdapter(listadapter);

        TextView totalBill = (TextView)Layout.findViewById(R.id.previous_order_total_bill);
        totalBill.setText(""+order.totalBill);


        window =dialog.getWindow();

        size = new Point();
        windowManager = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
            windowManager.getDefaultDisplay().getSize(size);

            width = size.x;
            height = size.y;
        }else{
            display = windowManager.getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();
        }



        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            lp.width=(int)(width/1.125);
            lp.height = (int)(height/1.5);

        } else {
            lp.width=width/2;
            lp.height = (int)(height/1.25);

        }



        lp.gravity = Gravity.CENTER;

        window.setAttributes(lp);

        dialog.setCancelable(false);
        dialog.show();
    }



    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

        dataFragment.setList(orderObjects);

        if(alertdialogid==1)
        {
            dataFragment.setAlertDialogId(alertdialogid);
        }
        else
        {

            if(dialogid==1) {
                dataFragment.setdialogId(dialogid);
                dataFragment.setPos(pos);
            }
            else
            {
                if(dialogid==-1)
                    dataFragment.setdialogId(dialogid);

            }
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        if(dialog!=null)dialog.dismiss();



    }


    public void close(View v)
    {
        dialogid=-1;
        dialog.dismiss();

    }

    public ArrayList<SavedOrder> getOrders() {

        orders = new ArrayList<SavedOrder>();

         int count=0;

        if(orderObjects.size()!=0)
        {
            for(JSONObject entry : orderObjects){
                SavedOrder order = new SavedOrder(entry,memberNumber,count);
                orders.add(order);
                count++;
            }
        }

        else
        {

            alertdialogid=1;




            AlertDialog.Builder builder = new AlertDialog.Builder(PreviousOrdersActivity.this);
            builder.setTitle(R.string.label_alertdialog_No_Previous_Orders_Found)
                    .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                            finish();

                        }
                    });




            dialog = builder.show();


        }




        return orders;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        MenuItem item_refresh = menu.findItem(R.id.action_refresh);

        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);

        item_home.setVisible(false);
        item_home.setEnabled(false);
        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case com.mobile.ict.cart.R.id.action_home:
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressLint("NewApi")
    public class FetchOrders extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;


        public FetchOrders(Context context, Map<String, String> map){
            this.context=context;
            itemsMap = map;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   lockScreenOrientation();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            String url=  "http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/search/getOrdersForMember?format=binary&status=processed&abbr="+abbr+"&phonenumber=91"+memberNumber+"&projection=default";


            JsonParser jParser = new JsonParser();
            String response = jParser.getJSONFromUrl(url,null,"GET",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

            return response;
        }

        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();



            if(response.equals("exception"))
            {
                Dialog dia;
                AlertDialog.Builder builder = new AlertDialog.Builder(PreviousOrdersActivity.this);

                builder.setTitle(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();

                            }
                        })

                        .setCancelable(false);

                dia=builder.create();
                dia.show();

            }
            else
            {

                orderObjects=new ArrayList<JSONObject>();


                try {
                    JSONObject orderList = new JSONObject(response);
                    JSONArray ordersArray = orderList.getJSONObject("_embedded").getJSONArray("orders");


                    for(int i=0;i<ordersArray.length();i++) {
                        orderObjects.add((JSONObject)ordersArray.get(i));
                    }

                    Message msg = Message.obtain();
                    handler.sendMessage(msg);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                   // e.printStackTrace();

                    pd.dismiss();
                    Dialog dia;
                    AlertDialog.Builder builder = new AlertDialog.Builder(PreviousOrdersActivity.this);

                    builder.setTitle(R.string.label_alertdialog_No_Previous_Orders_Found)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();

                                }
                            })

                            .setCancelable(false);

                    dia=builder.create();
                    dia.show();
                }





            }

           // unlockScreenOrientation();


        }



    }


    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

}

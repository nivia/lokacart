package com.mobile.ict.cart;



        import android.app.Activity;
        import android.os.Bundle;
        import android.webkit.WebView;

        import java.util.Locale;



public class UserManualActivity extends Activity
        {
                WebView wv;
                String currentLanguage;
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                wv = new WebView(this);
                setContentView(wv);

           currentLanguage=Locale.getDefault().getLanguage();
                StringBuilder html = new StringBuilder();

                            if(currentLanguage.equals("hi")||currentLanguage.equals("mr"))
                            {

                                    html.append("<html lang=\"hi\">\n" +
                                            "    <head>\n" +
                                            "        <meta http-equiv=\"Content-Language\" content=\"hi\">\n" +
                                            "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                                            "        <h2></strong>कुटीर उद्योग एप्लिकेशन का उपयोग</h2>\n" +
                                            "    </head>\n" +
                                            "    <body>\n" +
                                            "        <a name=\"Top\"></a>\n" +
                                            "        <p>इस एप्लिकेशन को आप अपने दरवाजे पर अपने भोजन के लिए सामग्री प्राप्त करने में मदद करने के लिए विकसित किया गया है। आप अपनी पसंद के स्थानीय किसान समूहों से आदेश कर सकते हैं।<br>इस दस्तावेज़ अनुप्रयोग का उपयोग करने के बारे में जानकारी शामिल है। यदि आपको मदद की जरूरत है कि जब भी आप यह देख सकते हैं।</p>\n" +
                                            "        <ul>\n" +
                                            "            <li><a href=\"#SignUp\" style=\"text-decoration:none\">रजिस्टर </a></li>\n" +
                                            "            <li><a href=\"#Login\" style=\"text-decoration:none\">लॉग इन करना</a></li>\n" +
                                            "            <li><a href=\"#Dashboard\" style=\"text-decoration:none\">डैशबोर्ड</a></li>\n" +
                                            "            <li><a href=\"#NewOrder\" style=\"text-decoration:none\">नया ऑर्डर देना</a></li>\n" +
                                            "            <li><a href=\"#PrevOrder\" style=\"text-decoration:none\">संसाधित ऑर्डरस</a></li>\n" +
                                            "            <li><a href=\"#CancelOrder\" style=\"text-decoration:none\">दर्ज किया हुआ ऑर्डर रद्द करना</a></li>\n" +
                                            "            <li><a href=\"#Draft\" style=\"text-decoration:none\">ड्राफ्ट ऑर्डरस सेव करना</a></li>\n" +
                                            "        </ul>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"SignUp\"><b>रजिस्टर</b><br></a> इस एप्लिकेशन के मुख्य पृष्ठ है रजिस्टर।<br><img src=\"hi_login.png\">\n" +
                                            "            <ul>\n" +
                                            "                <li>स्टेप १: \"<b>साइन अप करें</b>\" बटन दबाएँ।</li>\n" +
                                            "                <li>स्टेप २: ओटीपी पाने के लिए \"<b>ओटीपी प्राप्त करें</b>\" बटन दबाएँ।  <br><img src=\"hi_getOtp.png\"></li>\n" +
                                            "                <li>स्टेप ३: ओटीपी प्राप्त होने पर दर्ज करें।</li>\n" +
                                            "                <li>स्टेप ४: \"कनफर्म\" बटन दबाने पर आपको साइन अप पेज पर लाया जाएगा।<br><img src=\"hi_signup.png\"></li>\n" +
                                            "                <li>स्टेप ५: फार्म भरने  के बाद \"<b>प्रस्तुत करें</b>\" पर क्लिक करें।</li>\n" +
                                            "                <li>स्टेप ६: अब एप्लिकेशन में लॉग इन कर सकते हैं<b> एडमिन अनुमोदन के बाद</b>।</li>\n" +
                                            "            </ul>\n" +
                                            "                <br><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Login\"><b>लॉग इन करना</b></a><br>सदस्य बनने के समय संगठन के साथ पंजीकृत फोन नंबर दर्ज करें।<br> \"लॉग इन ” बटन दबाएँ।<br> यदि आप अपने खाते का उपयोग नहीं कर सकते हैं तो इस मामले में, आप \"खाते का उपयोग नहीं कर सकता\" बटन पर क्लिक करके और चरणों का पालन करके अपना पासवर्ड बदल सकते हैं।<br>\n" +
                                            "            <a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Dashboard\"><b>डैशबोर्ड</b></a><br> यह इस एप्लिकेशन का होम इंटरफेस है।<br><img src=\"hi_dashboard.png\">\n" +
                                            "            <br> एक ऑर्डर देने के लिए आप \\\"+\\\" बटन दबाएँ जो शीर्ष बाएं कोने पर हैं, संगठन से जुड़ने के लिए। चेक बॉक्स के माध्यम से चुनें। आप केवल एक बार अनुरोध भेज सकते हैं। इसलिए संगठन बाद में सूची में प्रदर्शित नहीं किया जाएगा।\n" +
                                            "            <br> संगठनों पर ताजा अपडेट पाने के लिए, \\\"<b>रेफ़्रेश्</b>\\\" बटन पर दबाएँ जो शीर्ष दाएं कोने पर हैं। संगठन ने  आपका अनुरोध स्वीकार कर लिया है तो आप ऑर्डर दे सकते हैं। \n" +
                                            "            <br>उपलब्ध कराई गई स्पिनर के द्वारा आप जिस संगठन से लेन-देन करना चाहते हैं वह चुनें।\n" +
                                            "            <br>ऑर्डर करने के लिए आगे बढ़ने से पहले यहां उचित मात्रा चुनें।\n" +
                                            "            <br>टाइटल बार में हेल्प बटन दबाने पर आप हमारे बारे में जान सकते हैं।\n" +
                                            "            <br> यह हेल्प पेज भी वहां मौजूद है, जो आप किसी भी समय उपयोग कर सकते हैं।\n" +
                                            "            <br><img src=\"hi_help_.png\"><br><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"NewOrder\"><b>नया ऑर्डर देना</b></a>\n" +
                                            "                <br>\n" +
                                            "                    <ul>\n" +
                                            "                        <li>स्टेप १: संगठन और समूह चुनें जहां से आप ऑर्डर देना चाहते हैं। (जैस<a href=\"#Dashboard\" style=\"text-decoration:none\"> यहां</a> उल्लेख किया गया है ।)</li>\n" +
                                            "                        <li>स्टेप २: डैशबोर्ड में \"<b>नई ऑर्डर</b>\" बटन पर दबाएँ। <br>यह नया ऑर्डर इंटरफ़ेस पर ले जाता है।<br><img src=\"hi_dashboard.png\"></li>\n" +
                                            "                        <li>स्टेप ३: ऑर्डर की प्रक्रिया में वर्गीकृत उपज पर क्लिक करें।<br><img src=\"hi_classifiedProduce.png\"></li>\n" +
                                            "                        <li>स्टेप ४: प्रत्येक मद की मात्रा दर्ज करें।</li>\n" +
                                            "                        <li>स्टेप ५: \"<b>आगे बढ़ें\"</b>\" बटन पर दबाएँ।</li>\n" +
                                            "                        <li>स्टेप ६: यहाँ अपनी पूरी ऑर्डर और भुगतान की जाने वाली राशि दिखाए जाते हैं।<br>आइटम की अपनी सूची में आदेश करने के लिए \"<b>चेक आउट</b>\" पर क्लिक करें।<br><img src=\"hi_confirm.png\"></li>\n" +
                                            "                        <li>स्टेप ७:  एक पुष्टिकरण पेज मिल जाएगा। \"<b>हो गया।</b>\"पर क्लिक करें।<br><img src=\"hi_submit.png\" alt=\"Google logo\"></li>\n" +
                                            "                    </ul>\n" +
                                            "                        <br><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "      \n" +
                                            "            <a name=\"PrevOrder\"><b>संसाधित ऑर्डरस</b></a><br><b>\"संसाधित ऑर्डर\" </b>\" बटन पर क्लिक करके आप संगठन द्वारा संसाधित किये गय है, और पहुँच गए हैं <b>या</b> आप तक पहुंचनेवाले हैं।<br><img src=\"hi_prev.png\"><br>ऑर्डर पर क्लिक करने के बाद,आपको आदेश का विस्तृत दृश्य मिलता है।<br><img src=\"hi_preview.png\"><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"CancelOrder\"><b>दर्ज किया हुआ ऑर्डर रद्द करना</b></a><br>उन ऑर्डरस को रद्द कर सकते हैं, जो तैयार नहीं किया गया है।<br>\n" +
                                            "            <ul>\n" +
                                            "<li>स्टेप १: \"<b>दर्ज किया हुआ ऑर्डर </b>\" बटन पर क्लिक करें।।<br><img src=\"hi_cancel.png\"></li>\n" +
                                            "                <li>स्टेप २: आप जिस ऑर्डर को रद्द करना चाहते है, उस ऑर्डर का ऑर्डर आईडी का पता लगाएं।<br><img src=\"hi_cancel.png\"></li>\n" +
                                            "                <li>स्टेप ३: आईडी पर क्लिक करने पर, उस ऑर्डर में आइटम्स का पता चलता है।<br><img src=\"hi_cancelview.png\"></li>\n" +
                                            "                <li>स्टेप ४: \"<b>ऑर्डर रद्द करे </b>\" बटन पर क्लिक करें। </li>\n" +
                                            "            </ul>\n" +
                                            "                <br><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Draft\"><b>ड्राफ्ट ऑर्डरस सेव करना</b></a><br>आप ड्राफ्ट के रूप में आइटम को बचा के रख सकता है, जो बाद में ऑर्डर करना चाहते हैं।<br><b>ड्राफ्ट ऑर्डरस सेव करना</b><br>\n" +
                                            "            <ul>\n" +
                                            "                <li>स्टेप १: डैशबोर्ड में \"<b>नई ऑर्डर</b>\" बटन पर दबाएँ और आइटम और मात्रा को चुनें।</li>\n" +
                                            "                <li>स्टेप २: \"<b>ड्राफ्ट</b>\" बटन पर क्लिक करें और आप जो ऑर्डर सेव कर रहे हैं, उन मदों की सूची मिलती है।<br><img src=\"hi_draftorder.png\"></li>\n" +
                                            "                <li>स्टेप ३: \"<b>हो गया</b>\" बटन पर दबाएँ, अपने ड्राफ्ट बचाने के लिए।\"<b> सुधारना</b>\" बटन पर दबाएँ, इसे बदलने के लिए।</li>\n" +
                                            "            </ul>\n" +
                                            "                <br><b>ड्राफ्ट ऑर्डरस रद्द करना या सेव करना </b><br>\n" +
                                            "            <ul>\n" +
                                            "                <li>स्टेप १: डैशबोर्ड में\"<b> ड्राफ्ट ऑर्डरस </b>\" बटन पर दबाएँ। यह ड्राफ्ट ऑर्डर इंटरफ़ेस पर ले जाता है।<br><img src=\"hi_draft.png\"></li>\n" +
                                            "                <li>स्टेप २: आप वह ऑर्डर चुनें जो द्द करना या बचाना चाहते हैं।</li><br><img src=\"hi_checkeddraft.png\"></li>\n" +
                                            "                <li>स्टेप ३: ऑर्डर करने के लिए\"<b>ऑर्डर</b>\" बटन पर दबाएँ।</li><br>रद्द करने के लिए \"<b>हटा दें</b>\" बटन पर दबाएँ।</li>\n" +
                                            "                <li>स्टेप ४: ऑर्डर आइटमस देखने के लिए, ऑर्डर पर दबाएँ।<br><img src=\"hi_draftview.png\"></li>\n" +
                                            "            </ul>\n" +
                                            "                <br><a href=\"#Top\" style=\"text-decoration:none\">ऊपर </a>\n" +
                                            "        </p>\n" +
                                            "    </body>\n" +
                                            "</html>\n");



                    }

                    else
                            {
                                    html.append("<html>\n" +
                                            "    <head>\n" +
                                            "        <h2></strong>Using the Cart App</h2>\n" +
                                            "    </head>\n" +
                                            "    <body>\n" +
                                            "        <a name=\"Top\"></a>\n" +
                                            "        <p>This app has been developed to help you get the ingredients for your food at your doorstep. You can order from the local farmer groups of your choice.<br>This document contains information on how to use the app. You can refer to it whenever you need help.</p>\n" +
                                            "        <ul>\n" +
                                            "            <li><a href=\"#SignUp\" style=\"text-decoration:none\">Register</a></li>\n" +
                                            "            <li><a href=\"#Login\" style=\"text-decoration:none\">Sign In</a></li>\n" +
                                            "            <li><a href=\"#Dashboard\" style=\"text-decoration:none\">The Dashboard </a></li>\n" +
                                            "            <li><a href=\"#NewOrder\" style=\"text-decoration:none\">Placing a new order</a></li>\n" +
                                            "            <li><a href=\"#PrevOrder\" style=\"text-decoration:none\">Looking at your processed orders</a></li>\n" +
                                            "            <li><a href=\"#CancelOrder\" style=\"text-decoration:none\">Cancelling an order that you placed</a></li>\n" +
                                            "            <li><a href=\"#Draft\" style=\"text-decoration:none\">Saving your orders in Drafts</a></li>\n" +
                                            "        </ul>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"SignUp\"><b>REGISTER</b></a>This is the main page of the app.<br><img src=\"login.png\">\n" +
                                            "            <br>\n" +
                                            "                <ul>\n" +
                                            "                    <li>STEP 1: Click the \"<b>Sign Up</b>\" button.</li>\n" +
                                            "                    <li>STEP 2: Enter the details and click \"<b>Get OTP</b>\" to get the OTP on your e-mail address.<br><img src=\"getOtp.png\"></li>\n" +
                                            "                    <li>STEP 3: Enter the the OTP received via your e-mail.</li>\n" +
                                            "                    <li>STEP 4: Click \"Confirm\" and you will be taken to the Sign Up page.<br><img src=\"signup.png\"></li>\n" +
                                            "                    <li>STEP 5: Fill in the form and click \"<b>Submit</b>\".</li>\n" +
                                            "                    <li>STEP 6: You can now log into the app once the <b>Admin approves your request</b>.</li>\n" +
                                            "                </ul>\n" +
                                            "            <br>\n" +
                                            "                <a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Login\"><b>SIGN IN</b></a><br>Enter the number and the password registered with the organisation at the time of becoming a member.\n" +
                                            "                <br>Press the \"Login\" button.\n" +
                                            "                    <br>In case you can't access your account, you can change your password by clicking on \"<b>Can't access your account?</b>\" button and following the steps.\n" +
                                            "                        <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Dashboard\"><b>THE DASHBOARD</b></a><br>This is the home interface of the app.\n" +
                                            "                <br><img src=\"dashboard.png\">\n" +
                                            "                 <br> To start placing an order you have to click on the \\\"+\\\" sign on the top left corner, which will help you get added to a Organisation's list. Via checkboxes send in your requests to various organisations. You can send a request only once, hence that organisation will not be displayed in the list subsequently.\n" +
                                            "                 <br><br>The refresh button provided on the top right corner has to be clicked to get updated with the status of the organisation. If the organisation has accepted your request the available produce from that organisation will be displayed and orders can be placed.\n" +
                                            "                    <br>The Spinner provided helps you choose the NGO through which you want to do the transaction.\n" +
                                            "                        <br>Choose the appropriate quantities here before proceeding to order.\n" +
                                            "                            <br><br>The title bar contains the \"Help\" menu button where you can get information regarding us.\n" +
                                            "                                <br>It also has the help document which you can refer to when in doubt.\n" +
                                            "                                    <br><img src=\"help_.png\">\n" +
                                            "                                        <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"NewOrder\"><b>PLACING A NEW ORDER</b></a>\n" +
                                            "                <br>\n" +
                                            "                    <ul>\n" +
                                            "                        <li>STEP 1: Choose the organisation and group from which you want to order. (as mentioned <a href=\"#Dashboard\" style=\"text-decoration:none\">here</a>)</li>\n" +
                                            "                        <li>STEP 2: Click on the \"<b>New Order</b>\" button in the Dashboard.<br>This takes you to the New Order Interface.\n" +
                                            "                            <br><img src=\"dashboard.png\"></li>\n" +
                                            "                        <li>STEP 3: Click on the classified produce to place an order.<br><img src=\"classifiedProduce.png\"></li>\n" +
                                            "                        <li>STEP 4: Enter the quantity of each item that you want to order.</li>\n" +
                                            "                        <li>STEP 5: Click on the \"<b>Proceed</b>\" button.</li>\n" +
                                            "                        <li>STEP 6: Here you are shown your full order and the amount to be paid.<br>Click on \"<b>Check Out</b>\" to order your list of items.\n" +
                                            "                            <br><img src=\"confirm.png\"></li>\n" +
                                            "                        <li>STEP 7: You will get a confirmation page. Click on \"<b>Done</b>\"\n" +
                                            "                            <br><img src=\"submit.png\" alt=\"Google logo\"></li>\n" +
                                            "                    </ul>\n" +
                                            "                        <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        \n" +
                                            "            <a name=\"PrevOrder\"><b>LOOKING AT THE PROCESSED ORDERS</b></a>\n" +
                                            "                <br>Clicking on the \"<b>Processed Orders</b>\" button helps you look at the orders that have been processed by the organisation, and have reached <b>or</b> are about to reach you.\n" +
                                            "                    <br><img src=\"prev.png\">\n" +
                                            "                        <br>On clicking each of the orders, you get a detailed view of the order.\n" +
                                            "                            <br><img src=\"prevview.png\">\n" +
                                            "                               \n" +
                                            "                                        <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"CancelOrder\"><b>CANCELLING AN ORDER THAT YOU HAVE PLACED</b></a>\n" +
                                            "                <br>Those orders that have not been processed by the NGO can be cancelled before the organisation processes it.\n" +
                                            "                    <br>\n" +
                                            "                        <ul>\n" +
                                            "  <li>STEP 1: Click on the \"<b>Placed Orders</b>\" button.</li>\n" +
                                            "                            <li>STEP 2: Find the Order ID of the order you want to cancel.<br><img src=\"cancel.png\"></li>\n" +
                                            "                            <li>STEP 3: Clicking on that ID shows the items in the order.<br><img src=\"cancelview.png\"></li>\n" +
                                            "                            <li>STEP 4: Click the \"<b>Cancel Order</b>\" to confirm your action.</li>\n" +
                                            "                        </ul>\n" +
                                            "                            <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "        <p>\n" +
                                            "            <a name=\"Draft\"><b>SAVING A DRAFT</b></a><br>You might want to save the items you would like to order later as drafts.\n" +
                                            "                <br><b>Saving a draft</b>\n" +
                                            "                    <br>\n" +
                                            "                        <ul>\n" +
                                            "                            <li>STEP 1: Click on the \"<b>New Order</b>\" button and choose the items and their quantities you want to save.</li>\n" +
                                            "                            <li>STEP 2: Click on \"<b>Draft</b>\" button and a you get the list of items you are saving.<br><img src=\"draftorder.png\"></li>\n" +
                                            "                            <li>STEP 3: Click on \"<b>Done</b>\" to get your Drafts saved, else click \"Modify\" to redraft it.</li>\n" +
                                            "                        </ul>\n" +
                                            "                            <br><b>Ordering or Deleting a draft</b>\n" +
                                            "                                <br>\n" +
                                            "                                    <ul>\n" +
                                            "                                        <li>STEP 1: Click on the \"<b>Draft Order</b>\" button on the Dashboard. The interface that opens shows your saved drafts.<br><img src=\"draft.png\"></li>\n" +
                                            "                                        <li>STEP 2: Choose the orders you want to order/delete by checking the boxes.</li><img src=\"checkeddraft.png\"></li>\n" +
                                            "                                        <li>STEP 3: Click on \"<b>Order</b>\" to get your order placed.</li>\n" +
                                            "                                            <br> Click on \"<b>Delete</b>\" to delete the draft orders selected.</li>\n" +
                                            "                                        <li> You can also click on the order to view it's details.<br><img src=\"draftview.png\"></li>\n" +
                                            "                                    </ul>\n" +
                                            "                                        <br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                                            "        </p>\n" +
                                            "    </body>\n" +
                                            "</html>\n");
                            }
                wv.loadDataWithBaseURL("file:///android_asset/", html.toString(), "text/html", "UTF-8", "");
            }
        }

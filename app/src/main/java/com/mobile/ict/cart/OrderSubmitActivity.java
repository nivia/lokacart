package com.mobile.ict.cart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class OrderSubmitActivity extends Activity implements View.OnClickListener{

    Button b1;
    TextView orderid;
    String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_submit);

        orderId= getIntent().getExtras().getString("orderid");
        orderid=(TextView)findViewById(R.id.message2);
        orderid.setText(getResources().getString(R.string.label_activity_ordersubmit_orderid)+": "+orderId);
        b1 = (Button) findViewById(R.id.button_order_submit_done);
        b1.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_home) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view==b1){
            Intent i = new Intent(this,DashboardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finishOrderSubmitActivity();
                    }
    }

    public void finishOrderSubmitActivity() {
        OrderSubmitActivity.this.finish();
    }

}
